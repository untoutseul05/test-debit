#!/bin/bash

# parametres
pgm_dir="/home/<user>/speedtest-cli/"
pgm_bin="<dirbin>"
log_name="debit.log"
log_file=${pgm_dir}${log_name}

# si le fichier n'existe pas, on le crée avec l'entête
if [ ! -f "${log_file}" ]; then
    echo "Date Heure;Ping;Desc.;Mont.;" >> ${log_file}
fi

# -- recuperation de l heure
now=$(date "+%d/%m/%y %H:%M")

# -- calcul
res=$(${pgm_bin} --simple | tr "\n" ";" | sed "s/Ping:\ //g")
res2=$(echo "$res" | sed "s/\ ms//g" | sed "s/Download:\ //g")
res=$res2

res2=$(echo "$res" | sed "s/\ Mbit\/s//g" | sed "s/Upload:\ //g")
res=$res2

res2=$(echo "$res" | tr "." ",")
res=$res2

echo "${now} - ${res}" >> ${log_file}
