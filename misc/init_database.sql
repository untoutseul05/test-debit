-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : sam. 09 oct. 2021 à 19:41
-- Version du serveur :  8.0.26-0ubuntu0.20.04.3
-- Version de PHP : 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `testdebit`
--
-- DROP DATABASE IF EXISTS `testdebit`;
CREATE DATABASE IF NOT EXISTS `testdebit` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `testdebit`;

CREATE USER IF NOT EXISTS 'testdebit'@'localhost' IDENTIFIED BY 'TestDebit2021$69260';
GRANT ALL PRIVILEGES ON testdebit . * TO 'testdebit'@'localhost';
FLUSH PRIVILEGES;

-- --------------------------------------------------------

--
-- Structure de la table `maxdate`
--

-- DROP TABLE IF EXISTS `maxdate`;
CREATE TABLE `maxdate` (
  `maxdate_id` int NOT NULL,
  `maxdate_date` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `maxdate_heure` varchar(5) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `mesure`
--

-- DROP TABLE IF EXISTS `mesure`;
CREATE TABLE `mesure` (
  `mesure_id` int UNSIGNED NOT NULL,
  `mesure_date` varchar(10) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `mesure_heure` varchar(5) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `mesure_ping` float NOT NULL,
  `mesure_dwn` float NOT NULL,
  `mesure_up` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `maxdate`
--
ALTER TABLE `maxdate`
  ADD PRIMARY KEY (`maxdate_id`);

--
-- Index pour la table `mesure`
--
ALTER TABLE `mesure`
  ADD PRIMARY KEY (`mesure_id`),
  ADD UNIQUE KEY `idx_date_heure` (`mesure_date`,`mesure_heure`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `maxdate`
--
ALTER TABLE `maxdate`
  MODIFY `maxdate_id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `mesure`
--
ALTER TABLE `mesure`
  MODIFY `mesure_id` int UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

