#!/bin/bash

###################################
#                                 #
#       Debit Internet V1.0       #
#      script de mise a jour      #
#                                 #
###################################

# test si le lancement est en sudo
echo "test lancement sudo..."
if [ "$USER" != "root" ] || [ -z "$SUDO_USER" ]; then
  echo "a lancer en sudo"
  exit
fi
echo "suite"

#recuperation du repertoire courant
repcrt=$(dirname $0)
echo "rep courant = '$repcrt'"

# on verifie le repertoire par defaut
test=$(cat /etc/apache2/sites-enabled/* | grep DocumentRoot | grep -v "#" | sed "s/\ //g"  | sed "s/\t//g" | sort -u)
test2=${test:12}
echo "rep apache par defaut ($nb) : '$test2'"
if [ ! -d "$test2" ]; then
  echo "le repertoire par defaut ($test2) inconnu."
else
  echo "le repertoire par defaut ($test2) existe."
  # on sauvegarde le repertoire debit_internet actuel s'il existe
  rep_source="${test2}/debit_internet"
  if [ -d "$rep_source" ]; then
    now=$(date +%Y%m%d%H%M%S)
    rep_dest="${rep_source}.${now}"
    echo "rep_dest='$rep_dest'"
    mv $rep_source $rep_dest
  else
    echo "Le '$rep_source' n'existe pas. pas besoin de sauvegarde."
  fi

  #echo "now='$now'"
  # on va extraire les fichiers du site web dans le repertoire par defaut
  if [ -d "$rep_source" ]; then
    echo "Le repertoire '$rep_source' existe. tar impossible"
  else
    #tar xzf misc/debit_internet.tar.gz -C $test2
    # Attention : il y a un probleme de repertoire --> il faut rectifier 
    #   les chemins comme pour les shells
    cp -r ${repcrt}/debit_internet $test2
    # il faut revoir le repertoire <user> --> utilisateur qui réalise l'install
    shellname="import_txt.php"
    modifiedfilename="${rep_source}/${shellname}"
    echo "modifiedfilename = '$modifiedfilename'"
    if [ -f "$modifiedfilename" ]; then
      # on cree le shell en remplacant <user> par sa valeur
      cat $modifiedfilename | sed "s/<user>/${SUDO_USER}/g" > $modifiedfilename.0
      rm $modifiedfilename
      mv $modifiedfilename.0 $modifiedfilename
      tsttmp=$(cat $modifiedfilename | grep ${SUDO_USER})
      if [ -n "$tsttmp" ]; then
        echo "modif '${SUDO_USER}' : Ok"
      else
        echo "modif '${SUDO_USER}' : Ko"
      fi
   else
     echo "fichier '$modifiedfilename' introuvable"
   fi
  fi
fi

# il faut modifier les repertoires (to do ?)

echo ""
echo "-- Installation des paquets"

# repertoire de mesure
rep="/home/${SUDO_USER}/speedtest-cli"
if [ ! -d "$rep" ]; then
  mkdir ${rep}
fi

# ajout des shells
echo "ajout test-debit.sh"
shellname1="speedtest-cli"
shellname2="test-debit.sh"

if [ ! -f "${rep}/${shellname2}" ]; then
  # on cree le shell en remplacant <user> par sa valeur
  # on remplace <dir> par le repertoire du speedtest-cli
  tmp=$(which speedtest-cli)
  echo "tmp = '$tmp'"
  cat misc/${shellname1}_${shellname2} | sed "s/<user>/${SUDO_USER}/g" > ${rep}/tmp_${shellname2}
  cat ${rep}/tmp_${shellname2} | sed "s|<dirbin>|${tmp}|g" > ${rep}/${shellname2}
  chmod +x ${rep}/${shellname2}
  rm ${rep}/tmp_${shellname2}
fi


# base de données
#  --> script de creation
#test=$(echo "show databases;" | mysql -u root -pEktwno\$2510 mysql 2> /dev/null | grep "testdebit")
#if [ -z "$test" ]; then
#  # on cree la bas et les tables
#  read -p "mot de passe root BdD : " mdp
#  echo "creation bdd et tables"
#  mysql -u root -p${mdp} < misc/init_database.sql
#else
#  echo "la BdD existe deja."
#fi

