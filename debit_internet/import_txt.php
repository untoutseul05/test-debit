<?php
  // page principale d'import des fichiers de mesure
  // V1.0 - 23/10/1969
  //  la date jalon permet de supprimer les lignes deja importees
  //  bug : la date de jalon doit être importee complement (et non date du jour) --> a tester
  //  evolution : il faut ajouter un top pour importer tous les enregistrements (au moins 1 fois)

  require ("import_txt_sub.php");

  echo "<!DOCTYPE html>\n";
  echo "<html lang=\"fr\">\n";
  echo "\t<head>\n";
  echo "\t\t<title>Import fichiers texte</title>\n";
  echo "\t\t<meta charset=\"utf-8\"/>\n";
  echo "\t</head>\n";
  echo "\t<body>\n";
  echo "\t\t<p>version du 02/11/2021 23h35</p>";
  echo "\t\t<p><u>Import fichiers texte</u><p>\n";
  echo "\t\t<p>&nbsp;</p>\n";
  echo "\t\t<a href=\"index.html\">retour index</a>\n";
  echo "\t\t<p>&nbsp;</p>\n";
  echo "\t\t<p>\n";

  // connexion à la base de données
  $servername = 'localhost';
  $username = 'testdebit';
  $password = 'TestDebit2021$69260';
  $dbname = 'testdebit';
  // repertoire de destination des mesures
  $rep = '/home/<user>/speedtest-cli/';

  //On établit la connexion
  $conn = new mysqli($servername, $username, $password, $dbname);

  //On vérifie la connexion
  if($conn->connect_error){
    die('Erreur : ' .$conn->connect_error);
  }
  echo "\t\t\tConnexion BdD réussie<br>\n";

  // on cherche les fichiers de type debit.yyyymmaa.log

  $dp = opendir($rep);

  while($entree = readdir($dp)){
    $fictot = "$rep/$entree";

    if(is_file("$fictot"))
    {
      if ((substr($entree, strlen($entree) - 4) == '.log') && (substr($entree,0,9) == 'debit.202')){
        // on passe les datas dans un tableau
        $ListFiles[$i]=$entree;
        $i++;
      }
    }
  }
  $nbel = $i;
  closedir($res);

  // tri le tableau par ordre croissant
  sort($ListFiles);

  // on recupère le max en base
  //-----------------------------------------

  // on cherche si la ligne est deja en base
  $sql = "SELECT maxdate_date, maxdate_heure from maxdate";
  $result = $conn->query($sql);
  if ($result->num_rows > 0)
  {
    // lecture de l'enregistrement
    while($row = $result->fetch_assoc())
    {
      $datebdd = $row["maxdate_date"];
      // on genere le fichier jalon
      $dateisojj = substr($datebdd,0,2);
      $dateisomm = substr($datebdd,3,2);
      $dateisoaa = "20" . substr($datebdd,6,2);
      $dateiso=$dateisoaa . $dateisomm . $dateisojj;
      $filenamejalon = "debit." . $dateiso . ".log";
      echo "\t\t\tfilenamejalon = '" . $filenamejalon . "'<br>\n";
    }
  }
  else
  {
    echo "\t\t\\t0 result<br>\n";
  }

  // on supprime les fichiers jusqu'à arriver à la date que l'on efface aussi
  echo "\t\t\t*** nb fichier av filtre : $nbel<br>\n";

  $i=0;
  $nbeltmp=0;
  while ( $i < $nbel)
  {
    $fictmp = $ListFiles[$i];

    //echo "--";
    //echo "  --> '" . $filenamejalon . "' vs '" . $fictmp . "'<br>";
    // La date jalon peut-être incomplete : on la selectionne
    if ("$fictmp" >= "$filenamejalon" )
    {
      echo "\t\t\t--<br>\n";
      echo "\t\t\ton laisse le fichier : '" . $fictmp . "'<br>\n";
      echo "\t\t\t--<br>\n";
      $ListFilestmp[$nbeltmp]=$fictmp;
      $nbeltmp++;
    }

    $i++;
  }

  // on recupère dans les vraies variables
  $ListFiles = $ListFilestmp;
  $nbel      = $nbeltmp;

  //----------------------------------------

  // on filtre les anciens fichiers

  echo "\t\t\t*** nb fichier ap filtre : $nbel<br>\n";
  echo "\t\t\tav appel fnct gestion_fichiers<br>\n";
  gestion_fichiers($ListFiles, $nbel,$conn,$rep);
  // on appelle pour la journée courante
  $tmpfic="debit.log";
  if (file_exists($rep . $tmpfic))
  {
    $FichierCrt[0] = $tmpfic;
    $nbtmp = 1;
    $retour = gestion_fichiers($FichierCrt,$nbtmp,$conn,$rep);
    echo "\t\t\tap appel fnct gestion_fichiers<br>\n";
    $lignedatemax  = $retour[0];
    $ligneheuremax = $retour[1];
  }
  
  echo "\t\t\t<br>\n";
  echo "\t\t\t----------------------------------------------------------<br>\n";
  echo "\t\t\t<br>\n";
  echo "\t\t\t +++ MAJ maxdate +++\n";
  echo "\t\t\t<br>\n";
  echo "\t\t\tlignedatemax  = '" . $lignedatemax  . "'<br>\n";
  echo "\t\t\tligneheuremax = '" . $ligneheuremax . "'<br>\n";

  // on ne met a à jour que si les 2 sont emplis --> sinon, pas de fichier a gerer
  if (("$lignedatemax" != "" ) && ("$ligneheuremax" != ""))
  {
    // on ajoute des quotes
    $lignedatemax2 = "'" . $lignedatemax . "'";
    $lignedatemax = $lignedatemax2;

    $ligneheuremax2 = "'" . $ligneheuremax . "'";
    $ligneheuremax = $ligneheuremax2;

  // on inscrit le max en base
  // on select le count pour savoir si on fait un insert ou un update
  $sql = "SELECT maxdate_date, maxdate_heure from maxdate";
  $result = $conn->query($sql);
  if ($result->num_rows > 0)
  {
    // on fait un update
    $sql = "UPDATE maxdate set maxdate_date = $lignedatemax, maxdate_heure = $ligneheuremax where maxdate_id = 1";
    echo "\t\t\tok\n";
  }
  else
  {
    // on fait un insert
    $sql= "INSERT INTO maxdate (maxdate_id, maxdate_date, maxdate_heure) VALUES (1, $lignedatemax, $ligneheuremax)";
    echo "\t\t\tko\n";
  }

  if ($conn->query($sql) === TRUE)
  {
    echo "\t\t\tNew record created / updated successfully<br>\n";
  }
  else
  {
    echo "\t\t\tError: " . $sql . "<br>\n\t\t\t" . $conn->error . "<br>\n";
  }
  }
  // on ferme la base de donnnes
  mysqli_close($conn);
  echo "\t\t</p>\n";  
  echo "\t</body>\n";
  echo "</html>\n";
?>
