<?php
  // fonction_fichiers
  //  V 1.0 - 23/10/2021
  //   pour chaque fichier, lecture d'une ligne (ie 1 mesure)
  //   et la met en base si elle n'existe pas (critere date - heure)
  function gestion_fichiers ($ListFiles, $nbel,$conn,$rep)
  {
    echo "\t\t\tdans fonction gestion_fichiers avec nbel = $nbel<br>\n";
    $i=0;
    while ( $i < $nbel)
    {
      echo "\t\t\t- fichier ($i)= '$ListFiles[$i]'<br>\n";
      $fictmp = $ListFiles[$i];
      $i++;
      $fictot = "$rep/$fictmp";
      echo "\t\t\tfictot = '$fictot'<br>\n";
      $res = @fopen($fictot, 'r');

      /*Tant que la fin du fichier n'est pas atteinte, c'est-à-dire
       *tant que feof() renvoie FALSE (= tant que !feof() renvoie TRUE)
       *on echo une nouvelle ligne du fichier*/
      while(!feof($res))
      {
        $ligne = fgets($res);
        if (($ligne != '') && (substr($ligne,0,4) != 'Date') && (strlen($ligne) > 20))
        {
          // si ligne vide, incomplète ou commence par Date, on saute*/  
          echo "\t\t\t --> " . $ligne . "<br>\n";
          // on decompose la ligne
          $lignedate  = substr($ligne,0,8);
          $ligneheure = substr($ligne,9,5);
          echo "\t\t\t  --> lignedate  = '$lignedate'<br>\n";
          echo "\t\t\t  --> ligneheure = '$ligneheure'<br>\n";
          $lignedata = substr($ligne,17);

          // ping
          $pos = strpos($lignedata,";");
          $ligneping = substr($lignedata,0,$pos);
          echo "\t\t\t  --> ligneping  = '$ligneping'<br>\n";
          $postmp = $pos + 1;

          // Download
          $pos = strpos($lignedata,";",$postmp);
          $lignedwn = substr($lignedata,$postmp,$pos - $postmp);
          echo "\t\t\t  --> lignedwn  = '$lignedwn'<br>\n";
          $postmp = $pos + 1;

          // upload
          $ligneup = substr($lignedata,$postmp,-2);
          echo "\t\t\t  --> ligneup  = '$ligneup'<br>\n";

          // on écrit la ligne
          $lignedatetmp  = "'" . $lignedate . "'";
          $ligneheuretmp = "'" . $ligneheure . "'";
          $lignepingtmp  = str_replace(",", ".", $ligneping);
          $lignedwn      = str_replace(",", ".", $lignedwn);
          $ligneup       = str_replace(",", ".", $ligneup);
          echo "\t\t\tavant query<br>\n";
          // si la ligne existe, on ne fait rien
          $sql="SELECT mesure_id FROM mesure where mesure_date = " . $lignedatetmp . " AND mesure_heure = " . $ligneheuretmp;
          echo "\t\t\tsql = '$sql'<br>\n";
          $result = $conn->query($sql);
          if ($result->num_rows > 0)
          {
            // l'enreg existe
            echo "\t\t\t'" . $lignedatetmp . "' - '" . $ligneheuretmp . "' existe --> RAF.<br>\n";
          }
          else
          {
            $sql1 = "INSERT INTO mesure (mesure_date, mesure_heure, mesure_ping, mesure_dwn, mesure_up)";
            $sql2 = " VALUES ($lignedatetmp, $ligneheuretmp, $lignepingtmp, $lignedwn, $ligneup)";
            $sql = $sql1 . $sql2;
            echo "\t\t\trequete = '" . $sql ."'<br>\t";
            if ($conn->query($sql) === TRUE)
            {
              echo "\t\t\tNew record created successfully<br>\n";
            }
            else
            {
              echo "\t\t\tError: " . $sql . "<br>" . $conn->error . "<br>\n";
            }

            // on garde la valeur date - heure (select ordre asc)
            $lignedatemax  = "'" . $lignedate . "'";
            $ligneheuremax = "'" . $ligneheure . "'";

            echo "\t\t\tdans sub : <br>\n";
            echo "\t\t\t  - lignedatemax  = $" . $lignedatemax . "$<br>\n";
            echo "\t\t\t  - ligneheuremax = $" . $ligneheuremax . "$<br>\n";
          }
          echo "\t\t\tapres query : " . $stmt . "<br>\n";
          echo "\t\t\t*** <br>\n";
        } // if ligne correcte
      } // while liste lignes
      fclose($res);
    } // while liste fichiers
    // on a finit les fichiers, on renvoie les maxima
    return array($lignedate,$ligneheure);
  }
?>

