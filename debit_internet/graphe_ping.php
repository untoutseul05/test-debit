<?php
  //recup du jour
  $date1 = $_GET [ 'date1' ];
  $datej = substr($date1,8,2);
  $datem = substr($date1,5,2);
  $datea = substr($date1,2,2);
  $datetmp = $datej ."/" . $datem . "/" . $datea;

  // connexion a  la BdD
  $host        = "localhost";
  $utilisateur = "testdebit";
  $motdepasse  = "TestDebit2021$69260";
  $base        = "testdebit";
  $conn = new mysqli($host,$utilisateur,$motdepasse,$base) or die('Connexion impossible: ' . mysql_error());

  //On vérifie la connexion
  if($conn->connect_error){
    die('Erreur : ' .$conn->connect_error);
  }
  //echo "Connexion BdD réussie<br>";

  $sql = "SELECT mesure_heure,mesure_ping from mesure where mesure_date = '" . $datetmp . "'";
  $result = $conn->query($sql);
  //echo "erreur select : '" . $conn->error . "'<br>";
  //echo "nb resultats = " . $result->num_rows . "<br>";

  $heure    = array();
  $ping     = array();
  $heuretmp = array();
  $pingtmp  = array();
  $i=0;

  while($row = $result->fetch_assoc())
  {
    //Mettre la ligne dans le tableau
    $heure[$i] = $row["mesure_heure"];
    $ping[$i]  = $row["mesure_ping"];
    //Prendre la première mesure comme minimum et maximum
    if($i==0)
    {
      $min=$row["mesure_ping"];
      $max=$row["mesure_ping"];
    }

    //Tester si la mesure est inférieure au minimum et le prendre s'il l'est
    if($row["mesure_ping"] < $min)
    {
      $min=$row["mesure_ping"];
    }

    //Tester si la mesure est inférieure au maximum et le prendre s'il l'est
    else
    {
      if($row["mesure_ping"] > $max)
      {
        $max=$row["mesure_ping"];
      }
    }
    $i++;
  }
  //echo "min : ". $min . "   -   max : " . $max;

  // on ajoute des enregistrements s'il manque des heures
  // on met -1 dans les valeurs pour dire invalide
  //echo "------------<br>";
  //echo "  enreg manquant<br>";

  // $i donne le nombre d'elements
  $i = 0;
  for ($idxtmp=0;$idxtmp<24;$idxtmp++)
  {
    // on a un enreg de plus
    $i++;

    // genere l'heure pour test de recherche
    $tmp=str_pad($idxtmp,2,"0",STR_PAD_LEFT) . ":02";
    //echo "gene heure : $tmp<br>";

    // si l'enregistrement existe, on le récupère sinon on met -1
    $tra = array_search($tmp,$heure,TRUE);
    if ($tra !==FALSE)
    {
      //echo "$tmp trouve<br>";
      $heuretmp[$idxtmp] = $tmp;
      $pingtmp[$idxtmp]  = $ping[$tra];
    }
    else
    {
      //echo "$tmp a cree<br>";
      $heuretmp[$idxtmp] = $tmp;
      $pingtmp[$idxtmp] = "-1";
    }
  }

  /*echo "------------<br>";
  echo "  verif enreg manquant<br>";
  foreach ($pingtmp as $idx => $quantite)
  {
    echo "heure = $heuretmp[$idx], $pingtmp[$idx]<br>";
  }
  echo "----<br>";*/

  //Type mime de l'image
  header('Content-type: image/png');
  //Chemin vers le police à utiliser
  $font_file = './arial.ttf';
  //Adapter la largeur de l'image avec le nombre de données
  $largeur=$i*50+90;
  $hauteur=400;
  //Hauteur de l'abscisse par rapport au bas de l'image
  $absis=80;
  //Création de l'image
  $courbe=imagecreatetruecolor($largeur, $hauteur);
  //Allouer les couleurs à utiliser
  $bleu=imagecolorallocate($courbe, 0, 0, 255);
  $ligne=imagecolorallocate($courbe, 220, 220, 220);
  $fond=imagecolorallocate($courbe, 250, 250, 250);
  $noir=imagecolorallocate($courbe, 0, 0, 0);
  $rouge=imagecolorallocate($courbe, 255, 0, 0);
  //Colorier le fond
  imagefilledrectangle($courbe,0 , 0, $largeur, $hauteur, $fond);
  //Tracer l'axe des abscisses
  imageline($courbe, 50, $hauteur-$absis, $largeur-10,$hauteur-$absis, $noir);
  //Tracer l'axe des ordonnées
  imageline($courbe, 50,$hauteur-$absis,50,20, $noir);
  //Decaler 10px vers le haut le si le minimum est différent de 0
  if($min!=0)
  {
    $absis+=10;
    $a=10;
  }
  //Nombres des grilles verticales
  $nbOrdonne=10;
  //Calcul de l'echelle des abscisses
  $echelleX=($largeur-100)/$i;
  //Calcul de l'echelle des ordonnees
  $echelleY=($hauteur-$absis-20)/$nbOrdonne;
  $i=$min;
  //Calcul des ordonnees des grilles
  $py=($max-$min)/$nbOrdonne;
  $pasY=$absis;
  while($pasY<($hauteur-19))
  {
    //Affiche la valeur de l'ordonnee
    imagestring($courbe, 2,10 , $hauteur-$pasY-6, round($i), $noir);
    //Trace la grille
    imageline($courbe, 50, $hauteur-$pasY, $largeur-20,$hauteur-$pasY, $ligne);
    //Decaler vers le haut pour la prochaine grille
    $pasY+=$echelleY;
    //Valeur de l'ordonnee suivante
    $i+=$py;
  }

  $j=-1;
  //Position du premier jour de production
  $pasX=90;
  //Parcourir le tableau pour le traçage du diagramme
  foreach ($pingtmp as $idx => $quantite)
  {
    //calculer la hauteur du point par rapport à sa valeur
    if ($quantite != -1)
    {
      $y=($hauteur) -(($quantite -$min) * ($echelleY/$py))-$absis;
      //dessiner le point
      imagefilledellipse($courbe, $pasX, $y, 6, 6, $rouge);
    }
    //Afficher l'heure avec une inclinaison de 315°
    imagefttext($courbe, 10, 315, $pasX, $hauteur-$absis+20, $noir, $font_file, $heuretmp[$idx]);
    // En haut à gauche : titre du graphe
    imagefttext($courbe, 10, 0, 10, 10, $noir, $font_file, "Ping (ms)");

    //Tacer une ligne veticale de l'axe de l'abscisse vers le point
    //imageline($courbe, $pasX, $hauteur-$absis+$a, $pasX,$y, $noir);
    if(($j!==-1) && ($quantite != -1) && ($quant_prev != -1))
    {
      //liée le point actuel avec le précédent
      imageline($courbe,($pasX-$echelleX),$yprev,$pasX,$y,$noir);
    }
    //Afficher la valeur au dessus du point
    if ($quantite != -1)
    {
      imagestring($courbe, 2, $pasX-15,$y-14 , $quantite, $bleu);
    }
    $j=$quantite;
    //enregister la hauteur du point actuel pour la liaison avec la suivante
    $yprev=$y;
    $quant_prev=$quantite;
    //Decaler l'abscisse suivante par rapport à son echelle
    $pasX+=$echelleX;
  }
  //Envoyer le flux de l'image
  imagepng($courbe);
  //Desallouer le memoire utiliser par l'image
  imagedestroy($courbe);
?>
