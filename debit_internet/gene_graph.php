<!-- Jean-Philippe LEMOINE -->
<!-- page principale generation graphes -->
<!-- Version 0.1 - 07/10/2021 -->

<html>
  <head>
    <title>G&eacute;n&eacute;ration graphes</title>
  </head>
  <body>
    <p align="center"><H1>G&eacute;n&eacute;ration graphes</H1></p>
    <a href="index.html">retour index</a>
    <p>&nbsp;</p>
    <form>
      <div>
        <?php
          // top pour savoir si on est en train de valider
           $top_suite=FALSE;
          // recupèration la veille de la date du jour (le jour actuel. les données ne sont pas basables)
          $today=date('Y-m-d');
          echo "today ='$today'<br>";

          // recuperation paramètre date - si vide date du jour
          $datedebut = $_GET [ 'datedebut' ];
          if ("$datedebut" == "")
          {
            $datedebut = $today;
          }
          else
          {
            $top_suite=TRUE;
          }
          echo "Date : <input type=\"date\" value=\"" . $datedebut . "\" min=\"2020-01-01\" max=\"" . $today . "\" name=\"datedebut\">\n";
        ?>
      </div>
      <div>
        <input type="submit">
      </div>
    </form>
    <?php
      require ("calcul_graphe.php");
      //echo "top = $top_suite<br>";
      if ($top_suite)
      {
        //echo "av calcul des graphes<br>";
        echo "--  graphes au $datedebut --<br>&nbsp;<br>" ;
        calcul_graphe($datedebut);
        //echo "ap calcul des graphes<br>";
      }
    ?>
  </body>
</html>
